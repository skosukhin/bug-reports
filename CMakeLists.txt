cmake_minimum_required(VERSION 3.18)
project(CrayBug LANGUAGES C Fortran)
add_executable(foo foo.c main.f90)
add_custom_command(TARGET foo POST_BUILD COMMAND foo)
