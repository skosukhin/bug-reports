CMake versions (at least) up to `3.26.3` fail to link mixed C/Fortran
executables correctly when using Cray compiler family version `15.0.1`.

Steps to reproduce on LUMI:
```console
$ module load PrgEnv-cray cce/15.0.1
...
$ cmake .
...
$ make
...
/home/jenkins/crayftn/craylibs/google-perftools/src/tcmalloc.cc:647] Attempt to free invalid pointer: 0x4225f0
...
```

The problem is that the executable is linked with the Fortran compiler to
`libc` without linking it to `libtcmalloc_minimal` (redefines the `malloc`
function) first.

The reason for that is that CMake fails to detect the list of libraries that the
Fortran compiler links to by default. In the case of Cray Fortran `15.0.0` (and
older), CMake correctly detects that the compiler links to `libc` with no extra
linker flags and does not append `-lc` to the linker command. In the case of
Cray `15.0.1`, CMake detects that the C compiler links to `libc` (but not to
`libtcmalloc_minimal`), fails to detect that the Fortran compiler links to
`libc` too and, therefore, appends `-lc` to the command when linking the
executable with the Fortran compiler. That leads to the executable calling the
wrong memory allocation/deallocation functions.

The reason for the CMake to fail the detection is that the verbose output (i.e.
`ftn -v test.f90`) of the Fortran compiler has significantly changed and does
not fit the assumptions that the
[`CMAKE_PARSE_IMPLICIT_LINK_INFO`](https://gitlab.kitware.com/cmake/cmake/-/blob/79499f9e71d1837a71a71446860a61f111b22fb5/Modules/CMakeParseImplicitLinkInfo.cmake#L17)
function relies on. The older version of Cray Fortran reported:
```console
...
/opt/cray/pe/cce/15.0.0/binutils/x86_64/x86_64-pc-linux-gnu/bin/ld ... -ltcmalloc_minimal ... -lc
```
The relevant output of Cray Fortran `15.0.1` is:
```console
/opt/cray/pe/cce/15.0.1/cce/x86_64/bin/cce_omp_offload_linker --verbose -- /opt/cray/pe/cce/15.0.1/binutils/x86_64/x86_64-pc-linux-gnu/bin/ld ... -ltcmalloc_minimal ... -lc
...
/opt/cray/pe/cce/15.0.1/binutils/x86_64/bin/ld --relocatable /tmp/cooltmpdir-DyI852/acc-targetend-tmp-cce-openmp.o -o /tmp/cooltmpdir-DyI852/acc-targetend-cce-openmp.o
...
/opt/cray/pe/cce/15.0.1/binutils/x86_64/x86_64-pc-linux-gnu/bin/ld ... -ltcmalloc_minimal ... -lc
```
The first and the third lines above provide the required information on the
linker flags. However, the first line is ignored because it doesn't start with
the known linker command (see
[here](https://gitlab.kitware.com/cmake/cmake/-/blob/79499f9e71d1837a71a71446860a61f111b22fb5/Modules/CMakeParseImplicitLinkInfo.cmake#L85))
but with `.../cce_omp_offload_linker`. The third line doesn't get parsed at all
because the second line does match the regular expression and the parsing stops
at that point. Unfortunately, the second line does not contain the required
linker flags and is not what should be considered by CMake at all.

A (hopefully temporary) workaround for the issue is to let CMake know that the
Fortran compiler links to `libc` by default by adding something like the
following to the project's `CMakeLists.txt`:
```cmake
if("${CMAKE_Fortran_COMPILER_ID}" STREQUAL "Cray" AND
   "${CMAKE_Fortran_COMPILER_VERSION}" VERSION_EQUAL "15.0.1")
  set(CMAKE_Fortran_IMPLICIT_LINK_LIBRARIES "${CMAKE_Fortran_IMPLICIT_LINK_LIBRARIES};c")
endif()
```
With the fix above it is at least possible to get the expected output for this
example project:
```console
$ cmake .
-- The C compiler identification is Clang 15.0.6
-- The Fortran compiler identification is Cray 15.0.1
-- Cray Programming Environment 2.7.19 C
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /opt/cray/pe/craype/2.7.19/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Cray Programming Environment 2.7.19 Fortran
-- Detecting Fortran compiler ABI info
-- Detecting Fortran compiler ABI info - done
-- Check for working Fortran compiler: /opt/cray/pe/craype/2.7.19/bin/ftn - skipped
-- Configuring done (3.1s)
-- Generating done (0.0s)
...
$ make
[ 33%] Building C object CMakeFiles/foo.dir/foo.c.o
[ 66%] Building Fortran object CMakeFiles/foo.dir/main.f90.o
[100%] Linking Fortran executable foo
Success
[100%] Built target foo
```
