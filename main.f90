program main

  interface
    subroutine foo() bind(c)
    end subroutine foo
  end interface

  call foo()

end program main
